'use strict';

const PLUS = "+";
const MINUS = "-";
const DIVIDE = "/";
const MULTIPLICATION = "*";

let firstNumber = 0;
let secondNumber = 0;
let arithmeticSign = '+';

function checkNumber(number) {
    if (!Number.isNaN(number) && Number.isInteger(number)) {
        return true;
    }

    return false;
}

function calc(firstNumber, secondNumber, arithmeticSign) {
    if(arithmeticSign === PLUS) {
        return firstNumber + secondNumber;
    } else if(arithmeticSign === MINUS) {
        return firstNumber - secondNumber;
    } else if(arithmeticSign === DIVIDE) {
        return firstNumber / secondNumber;
    } else {
        return firstNumber * secondNumber;
    }
}

while (true) {
    firstNumber = prompt("Введіть перше число", firstNumber);
    if(firstNumber === null) {
        break;
    }

    firstNumber = Number(firstNumber);

    secondNumber = prompt("Введіть друге число", secondNumber);
    if(secondNumber === null) {
        break;
    }
    secondNumber = Number(secondNumber);

    arithmeticSign = prompt("Введіть математичну операцію для виконання", arithmeticSign);
    if(arithmeticSign === null) {
        break;
    }

    if (!checkNumber(firstNumber) || !checkNumber(secondNumber)) {
        alert("Ви ввели не валідні дані");

        continue;
    }

    if(arithmeticSign === PLUS) {
        break;
    } else if(arithmeticSign === MINUS) {
        break;
    } else if(arithmeticSign === DIVIDE && firstNumber !== 0 && secondNumber !== 0) {
        break;
    } else if(arithmeticSign === MULTIPLICATION) {
        break;
    } else {
        alert("Ви ввели не валідні дані");
    }
}

if(firstNumber !== null && secondNumber !== null && arithmeticSign !== null) {
    alert("Result: " + calc(firstNumber, secondNumber, arithmeticSign));
}

/*відповіді на теорію
*1 функції дозволяють робити однакові дії багато разів без повторення коду.
*2 Аргументи передаються у функцію для того, щоб повернути значення при виклику функції
* 3 Оператор return - повертає значення функції. За його допомогою повертається результат виконання функції.
якщо оператор return не використовується тоді функція повертає undefined.*/